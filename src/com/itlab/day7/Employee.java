package com.itlab.day7;

public class Employee {
	String FirstName;
	String LastName;
	int id;

	Employee(String FirstName, String LastName, int id) {
		this.FirstName = FirstName;
		this.LastName = LastName;
		this.id = id;

	}

	public Employee() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Employee [FirstName=" + FirstName + ", LastName=" + LastName + ", id=" + id + "]";
	}
	
	

}
