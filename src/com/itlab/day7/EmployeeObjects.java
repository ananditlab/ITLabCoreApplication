package com.itlab.day7;

import java.util.ArrayList;

public class EmployeeObjects {
	public static void main(String args[]) {
		ArrayList<Employee> AlObj = new ArrayList<Employee>();
		
		Employee Emp1 = new Employee("Joey", "Tribbiani", 1);
		System.out.println("FirstName:" + Emp1.FirstName + Emp1.LastName + "ID:" + Emp1.id);

		 AlObj.add(Emp1);
		Employee Emp2 = new Employee("Chandler", "Bing", 2);
		System.out.println("Name:" + Emp2.FirstName + Emp2.LastName + "ID:" + Emp2.id);
		AlObj.add(Emp2);
		Employee Emp3 = new Employee("Ross", "Geller", 3);
		System.out.println("Name:" + Emp3.FirstName + Emp3.LastName + "ID:" + Emp3.id);
		AlObj.add(Emp3);
		Employee Emp4 = new Employee("Rachel", "Greene", 4);
		System.out.println("Name:" + Emp4.FirstName + Emp4.LastName + "ID:" + Emp4.id);
		AlObj.add(Emp4);
		Employee Emp5 = new Employee("Monica", "Geller", 5);
		System.out.println("Name:" + Emp5.FirstName + Emp5.LastName + "ID:" + Emp5.id);
		AlObj.add(Emp5);
		Employee Emp6 = new Employee("Phoebe", "Buffay", 6);
		System.out.println("Name:" + Emp6.FirstName + Emp6.LastName + "ID:" + Emp6.id);
		AlObj.add(Emp6);
		Employee Emp7 = new Employee("Gunther", "Central Perk", 7);
		System.out.println("Name:" + Emp7.FirstName + Emp7.LastName + "ID:" + Emp7.id);
		AlObj.add(Emp7);
		Employee Emp8 = new Employee("Estelle", "Leonard", 8);
		System.out.println("Name:" + Emp8.FirstName + Emp8.LastName + "ID:" + Emp8.id);
		AlObj.add(Emp8);
		Employee Emp9 = new Employee("Jack", "Geller", 9);
		System.out.println("Name:" + Emp9.FirstName + Emp9.LastName + "ID:" + Emp9.id);
		AlObj.add(Emp9);
		Employee Emp10 = new Employee("Judy", "Geller", 10);
		System.out.println("Name:" + Emp10.FirstName + Emp10.LastName + "ID:" + Emp10.id);
		AlObj.add(Emp10);
System.out.println("size of list:"+AlObj.size());


	}
}