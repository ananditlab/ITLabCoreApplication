package com.itlab.day7;

import java.util.ArrayList;

public class ArryListExampleDay7 {

	public static void main(String[] args) {

		ArrayList<String> arrayListObj = new ArrayList<String>();

		arrayListObj.add("Abc");	
		arrayListObj.add("adsfd");
		arrayListObj.add("asdfasd");
		arrayListObj.add("werwerAbc");
		arrayListObj.add("sdfsdf");
		arrayListObj.add("xcvxc");
		arrayListObj.add("zxvxc");
		arrayListObj.add("zxcvcx");
		arrayListObj.add("zxcvcxvxvxcvAbc");
		arrayListObj.add("zxcvxc");
		arrayListObj.add("zxcvcxvxvxcvAbc");
		arrayListObj.add("zxcvxc");

		arrayListObj.add("zxcvcxvxvxcvAbc");
		arrayListObj.add("zxcvxc");

		arrayListObj.add("zxcvcxvxvxcvAbc");
		arrayListObj.add("zxcvxc");

		arrayListObj.add("zxcvcxvxvxcvAbc");
		arrayListObj.add("zxcvxc");

		arrayListObj.add("zxcvcxvxvxcvAbc");
		arrayListObj.add("zxcvxc");

		arrayListObj.add("zxcvcxvxvxcvAbc");
		arrayListObj.add("zxcvxc");

		System.out.println("ArryListExampleDay7.main(size)" + arrayListObj.size());

		System.out.println("ArryListExampleDay7.main(element at index 9)" + arrayListObj.get(9));

		arrayListObj.remove(8);

		System.out
				.println("ArryListExampleDay7.main(after removing element the size of array is)" + arrayListObj.size());

		// for (int i = 0; i < arrayListObj.size(); i++) {
		// System.out.println("ArryListExampleDay7.main()" +
		// arrayListObj.get(i));
	}

}

// }
