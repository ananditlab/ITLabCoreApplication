package com.itlab.day3;
public class Conversion {

	public static void main(String[] args) {
		int i = 123456789;
		float f = 43.25F;
		System.out.println("i is :" + i);
		System.out.println("f is :" + f);
		System.out.println("Conversion is :");
		int k = (int) f;
		float l = (float) i;

		System.out.println("int to  float=" + l);
		System.out.println("float to  int=" + k);

	}

}
