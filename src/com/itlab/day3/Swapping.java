package com.itlab.day3;
import java.util.Scanner;

public class Swapping {
	public static void main(String args[]) {
		int num1, num2;
		Scanner s = new Scanner(System.in);
		System.out.print("enter first number:");
		num1 = s.nextInt();
		System.out.print("enter second number:");
		num2 = s.nextInt();
		num1 = num1 ^ num2;
		num2 = num1 ^ num2;
		num1 = num1 ^ num2;

		s.close();
		System.out.println("the first number after swapping:" + num1);
		System.out.println("the second number after swapping:" + num2);
	}
}