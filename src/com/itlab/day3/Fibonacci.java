package com.itlab.day3;

public class Fibonacci {

	private static int i;

	public static void main(String[] args) {
		int f1 = 0, f2 = 1;
		for (i = 0; i < 30; i++) {
			f1 = f2;
			f2 = f1 + f2;
			System.out.println("" + f1);
		}
	}
}
