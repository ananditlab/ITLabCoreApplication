package com.itlab.day3;
import java.util.Scanner;

public class LeapYear {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		int n;
		System.out.println("enter the year:");
		n = sc.nextInt();
		if (n % 100 == 0) {
			if (n % 400 == 0)
				System.out.println("It is a leap year...");
			else
				System.out.println("it is not a leap year...");
		} else if (n % 4 == 0)
			System.out.println("it is a leap year...");
	}

}
