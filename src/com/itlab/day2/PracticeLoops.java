package com.itlab.day2;
public class PracticeLoops {
	public static void printnums(int n) {
		int j, i, num;
		for (i = 0; i < n; i++) {
			num = 1;
			for (j = 0; j < n; j++) {
				System.out.println(num + " ");
				num++;
			}
			System.out.println();
		}
	}

	public static void main(String args[]) {
		int n = 5;
		printnums(n);
	}
}
