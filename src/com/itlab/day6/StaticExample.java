package com.itlab.day6;

public class StaticExample {
	static int age;
	static String name;

	static void showInfo() {
		System.out.println("age is=" + age);
		System.out.println("name is=" + name);

	}

	public static void main(String args[]) {
		age = 50;
		name = "joey";
		showInfo();
	}
}
