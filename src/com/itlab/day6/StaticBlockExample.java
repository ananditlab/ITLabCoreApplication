package com.itlab.day6;

public class StaticBlockExample {
	static int a;

	static {
		a = 2;
	}

	public static void main(String args[]) {
		System.out.println("value of a is" + a);

	}
}
