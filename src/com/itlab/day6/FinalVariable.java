package com.itlab.day6;

public class FinalVariable {

	public static void main(String[] args) {
		final String abc = "FinalVariable";
		final int a = 10;
		final int b;
		final int c = 10;
		System.out.println("Final Variable is =" + abc);
		b = c + a;
		System.out.println("Final sum of Int =" + b);
	}

}
