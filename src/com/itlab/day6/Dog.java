package com.itlab.day6;
interface MyInterface {
	void makeSound();
}

public class Dog implements MyInterface {

	public void makeSound() {
		System.out.println("bark");

	}

}

class Cat implements MyInterface {

	public void makeSound() {
		System.out.println("Meow");

	}

}

class MainImplementation {
	public static void main(String args[]) {
		Dog objDog = new Dog();
		objDog.makeSound();
		Cat objCat = new Cat();
		objCat.makeSound();

	}

}
