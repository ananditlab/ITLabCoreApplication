package com.itlab.day6;
abstract class AbstractExample {
	abstract void move();

	abstract void stop();
}

class Wheel extends AbstractExample {
	void move() {
		System.out.println("Moves when in rest...");
	}

	@Override
	void stop() {
		System.out.println("Stops when moving..");

	}

}

class MainAbstractClass {
	public static void main(String args[]) {

		AbstractExample Ae;

		Ae = new Wheel();
		Ae.move();
		Ae.stop();

	}

}