package com.itlab.day6;

public class FinallyBlockExample {
	public static void main(String args[]) {
		int d, a;
		try {
			d = 0;
			a = 10 / d;
		} catch (ArithmeticException e) {
			System.out.println("division by zero...causes arithmetic exception.");
		} finally {
			System.out.println("Exception thrown...");
		}
	}
}
