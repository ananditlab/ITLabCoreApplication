package com.itlab.day6;

public class StaticClassExample {
	private static String abc = "alphabets";

	public static class Alphabets {
		public void print() {
			System.out.println("there are 26" +abc);

		}
	}

	public static void main(String args[]) {
		StaticClassExample.Alphabets objAlp = new StaticClassExample.Alphabets();
		objAlp.print();

	}
}