package com.itlab.day6;
interface InterfaceVehicle {
	void moveForward();

	void moveBackward();

}

public class Vehicle implements InterfaceVehicle {

	@Override
	public void moveForward() {
		System.out.println("BMW can move forward...");

	}

	@Override
	public void moveBackward() {
		// TODO Auto-generated method stub

	}

}

class Vehicle2 implements InterfaceVehicle {

	@Override
	public void moveForward() {
		System.out.println("Datsun can move forward ...");
	}

	@Override
	public void moveBackward() {
		System.out.println("Datsun can move bacckwards too..");

	}

}

class MainClass {
	public static void main(String args[]) {
		Vehicle objVeh = new Vehicle();
		objVeh.moveForward();
		Vehicle2 objVeh2 = new Vehicle2();
		objVeh2.moveForward();
		objVeh2.moveBackward();
	}
}
