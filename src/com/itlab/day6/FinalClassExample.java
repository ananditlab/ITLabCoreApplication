package com.itlab.day6;

final class FinalClassExample {
	void getName() {
		System.out.println("Name is joey");
	}
}

class Final {
	void getAge() {
		System.out.println("Age is 50");
	}
}

class MainDeclaration {
	public static void main(String args[]) {
		FinalClassExample objFCE = new FinalClassExample();
		objFCE.getName();
		Final objF = new Final();
		objF.getAge();
	}

}