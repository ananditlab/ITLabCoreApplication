package com.itlab.day6;

public class StaticMethodExample {
	public static void main(String atgs[]) {
		Example objEx = new Example();
		objEx.staticMethod();

	}
}

class Example {
	int a;
	static int b;
	String abc = "joey";
	static String xyz = "chandler";

	Example() {

		b++;
	}

	public void staticMethod() {
		System.out.println("first string is" + abc + a);
		System.out.println("second string is" + xyz + b);

	}

}