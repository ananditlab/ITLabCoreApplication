package com.itlab.day5;
public class WhileLoopStatement {
	public static void main(String args[]) {
		int i = 10;
		i = 2;
		while (i > 1) {
			System.out.println(i);
			i++;
		}
	}
}
