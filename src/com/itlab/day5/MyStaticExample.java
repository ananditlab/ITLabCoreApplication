package com.itlab.day5;

public class MyStaticExample {

	public String empName = "Abc";
	public static String empLastName = "XYZ";

	static {
		System.out.println("MyStaticExample.enclosing_method()");
		System.out.println("MyStaticExample.enclosing_method()");
		System.out.println("MyStaticExample.enclosing_method()");
		System.out.println("MyStaticExample.enclosing_method()");
		System.out.println("MyStaticExample.enclosing_method()");
	}

	public static void getMyStaticName() {
		System.out.println("MyStaticExample.getMyStaticName()" + empLastName);
	}

	public void getMyNonStaticName() {
		System.out.println("MyStaticExample.getMyNonStaticName()");
		getMyStaticName();
	}
}