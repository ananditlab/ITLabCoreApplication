package com.itlab.day5;

public class MyFinalExampleMainClass {

	public final static double PI_VALUE = 3.14;

	public static void main(String[] args) {

		System.out.println("MyFinalExampleMainClass.main()" + PI_VALUE);
	}
}