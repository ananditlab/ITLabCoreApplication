package com.itlab.day4;
class BoxTest {

	public static void main(String args[]) {
		Box b1 = new Box();

		Box b2 = new Box(2, 5, 10);

		Box b3 = new Box(10);

		Box b4 = new Box(b2);

		System.out.println("Volume is" + b1.volume());
		System.out.println("Volume is" + b2.volume());
		System.out.println("Volume is" + b3.volume());
		System.out.println("Volume is" + b4.volume());
	}
}