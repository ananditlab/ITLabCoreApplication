package com.itlab.day8;

import java.util.LinkedList;
import java.util.Queue;

public class MyQueueExample {
	public static void main(String args[]) {
		Queue<String> myQueue = new LinkedList<String>();
		myQueue.add("hello");
		myQueue.add("world");
		myQueue.add("java");
		myQueue.add("is");
		myQueue.add("fun");
		System.out.println("Queue is:" + myQueue);
		System.out.println("size of Queue is:" + myQueue.size());
		System.out.println("[remove()]removing the first element:" + myQueue.remove());
		System.out.println(
				"[element()]after removing the first element, the next element will shift to the first position to return that element without removing it from the list, the method is:"
						+ myQueue.element());
		System.out
				.println("[poll()]this method will remove and return the first element of the Queue:" + myQueue.poll());
		System.out.println(
				"[peek()]to see the next element after removing the first ones, the method is:" + myQueue.peek());
		System.out.println("now after all the operations the remaining elements are:" + myQueue);

	}
}
