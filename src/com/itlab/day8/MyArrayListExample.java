package com.itlab.day8;

import java.util.ArrayList;

public class MyArrayListExample {
	public static void main(String args[]) {
		ArrayList<String> myArrayList = new ArrayList<String>();
		myArrayList.add("Hello");
		myArrayList.add("world");
		myArrayList.add("java");
		myArrayList.add("is ");
		myArrayList.add("interesting.");
		myArrayList.size();
		System.out.println("ArrayList is:" + myArrayList);
		System.out.println("size of ArrayList is:" + myArrayList.size());
		myArrayList.remove(0);
		System.out.println("size of ArrayList after removing element: " + myArrayList.size());
		System.out.println("element at index 3 after removing element is:" + myArrayList.get(3));
	}
}
