package com.itlab.day8;

import java.util.Comparator;

public class StudentSortByName implements Comparator<Student> {

	@Override
	public int compare(Student arg0, Student arg1) {
		return arg0.fname.compareTo(arg1.fname);
	}
	
	

}
