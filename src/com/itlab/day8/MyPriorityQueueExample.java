package com.itlab.day8;

import java.util.PriorityQueue;

public class MyPriorityQueueExample {

	public static void main(String[] args) {
		PriorityQueue<String> myPriorityQ = new PriorityQueue<String>();
		myPriorityQ.add("java");
		myPriorityQ.add("is");
		myPriorityQ.add("fun");
		myPriorityQ.add("world");
		myPriorityQ.add("hello");
		System.out.println("size before operations is:" + myPriorityQ.size());
		System.out.println("remove created:" + myPriorityQ.remove("fun"));
		System.out.println("add this element in list:" + myPriorityQ.offer("wow"));
		System.out.println("add this element in list:" + myPriorityQ.offer("good"));
		System.out.println("Queue after operations:" + myPriorityQ);
		System.out.println("size after operations is:" + myPriorityQ.size());

	}

}
