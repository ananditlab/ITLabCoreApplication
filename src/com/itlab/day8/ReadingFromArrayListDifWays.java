package com.itlab.day8;

import java.util.ArrayList;
import java.util.Arrays;

public class ReadingFromArrayListDifWays {

	public static void main(String[] args) {

		Employee employeeObj1 = new Employee("Joey", "Tribbiani", 1);
		Employee employeeObj2 = new Employee("Chandler", "Bing", 2);
		Employee employeeObj3 = new Employee("Ross", "Geller", 3);
		
		ArrayList<Employee> employeeArrayListObj = new ArrayList<Employee>(Arrays.asList(employeeObj1, employeeObj2, employeeObj3));
		
		
		// Simple For Loop
		for (int i = 0; i<employeeArrayListObj.size();i++){
			Employee objectFromArraylist = employeeArrayListObj.get(i);
			System.out.println("ReadingFromArrayListDifWays.main() "+ i +" - "+ objectFromArraylist.toString());
		}

		// Advance for Loop
		for (Employee obj : employeeArrayListObj){
			System.out.println("ReadingFromArrayListDifWays.main() " + obj.toString());
		}
		
		
	}

}
