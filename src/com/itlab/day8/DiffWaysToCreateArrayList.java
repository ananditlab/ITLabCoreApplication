package com.itlab.day8;

import java.util.ArrayList;
import java.util.Arrays;

public class DiffWaysToCreateArrayList {

	public static void main(String[] args) {

		Employee employeeObj1 = new Employee("Joey", "Tribbiani", 1);
		Employee employeeObj2 = new Employee("Chandler", "Bing", 2);
		Employee employeeObj3 = new Employee("Ross", "Geller", 3);
		
		// Array.asList
		ArrayList<Employee> employeeArrayListObj = new ArrayList<Employee>(Arrays.asList(employeeObj1, employeeObj2, employeeObj3));
		System.out.println("DiffWaysToCreateArrayList.main()"+ employeeArrayListObj.size());
		
		// Anonymous inner class
		ArrayList<Employee> empployeeInnerClassObj = new ArrayList<Employee>(){{
			add(employeeObj1);
			add(employeeObj2);
			add(employeeObj3);
		}};
		
		System.out.println("DiffWaysToCreateArrayList.main() Size of Anonymous Class array "+empployeeInnerClassObj.size());
		
	}// close main methos

}//close class
