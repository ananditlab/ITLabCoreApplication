package com.itlab.day8;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class MyIteratorExample {
	public static void main(String[] args) {
		List<String> myIter = new LinkedList<>();
		myIter.add("hello");
		myIter.add("world");
		myIter.add("java");
		myIter.add("is");
		myIter.add("fun");
		System.out.println("to remove element from the list use [remove()] method." +"-the removed element is-"+ myIter.remove(1));
		System.out.println("element at index 3 is" + myIter.get(3));

		Iterator<String> iter = myIter.iterator();

		while (iter.hasNext()) {
			System.out.println(iter.next());
		}

	}
}