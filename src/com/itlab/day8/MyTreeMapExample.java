package com.itlab.day8;

import java.util.Collection;
import java.util.Iterator;
import java.util.TreeMap;

public class MyTreeMapExample {

	public static void main(String[] args) {
		TreeMap<String,String> myTreeMap=new TreeMap<String,String>();
	myTreeMap.put("1","java");
	myTreeMap.put("2","is");
	myTreeMap.put("3","fun");
	Collection c=myTreeMap.values();
	Iterator i=c.iterator();
	while(i.hasNext()){
		System.out.print(i.next()+"_");
	}

	}

}
