package com.itlab.day8;

import java.util.Hashtable;

public class MyHashTableExample {
	public static void main(String args[]) {
		Hashtable<Number, String> myHashTable = new Hashtable<Number, String>();
		myHashTable.put(1, "hello");
		myHashTable.put(2, "world");
		myHashTable.put(3, "java");
		myHashTable.put(4, "is");
		myHashTable.put(5, "fun");
		System.out.println("size is:" + myHashTable.size());
		System.out.println("gives the first entry" + " -->" + myHashTable.get(1));
		System.out.println("Hashtable contents are-->" + myHashTable);

	}
}
