package com.itlab.day8;

import java.util.Iterator;
import java.util.Vector;

public class MyVectorExample {

	public static void main(String[] args) {
		Vector<String> myVector = new Vector<String>();
		myVector.add("J");
		myVector.add("A");
		myVector.add("V");
		myVector.add("A");
		System.out.println("Vector list is:" + myVector);
		myVector.add("A");
		System.out.println("Vector list after adding is:" + myVector);
		myVector.remove(4);
		System.out.println("Vector list after removing element:" + myVector);
		
		
		
	}

}
