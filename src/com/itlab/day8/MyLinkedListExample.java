package com.itlab.day8;

import java.util.LinkedList;

public class MyLinkedListExample {
	public static void main(String args[]) {
		LinkedList<String> myLinkedList = new LinkedList<String>();
		myLinkedList.add("hello");
		myLinkedList.add("world");
		myLinkedList.add("java");
		myLinkedList.add("is");
		myLinkedList.add("fun");
		System.out.println("LinkedList is:" + myLinkedList);
		myLinkedList.addFirst("hey");
		myLinkedList.addLast("bbye.");
		System.out.println("LinkedList after adding elements:" + myLinkedList);
		Object firstvar = myLinkedList.get(2);
		System.out.println("Second Element is:" + firstvar);
		myLinkedList.set(0, "hellloooo");
		Object firstvar2 = myLinkedList.get(0);
		System.out.println("First element is:" + firstvar2);
		myLinkedList.remove(4);
		myLinkedList.removeFirst();
		myLinkedList.removeLast();
		System.out.println("LinkedList after removing elements:" + myLinkedList);
		myLinkedList.add(0, "java");
		myLinkedList.remove(3);
		System.out.println("Final LinkedList:" + myLinkedList);
	}
}
