package com.itlab.day8;

import java.util.ArrayList;
import java.util.Collections;

public class SortArrayListExample {

	public static void main(String[] args) {

		
		ArrayList <String> strArrayListObj = new ArrayList<String>();
		strArrayListObj.add("Zbc");
		strArrayListObj.add("Pbc");
		strArrayListObj.add("Cbc");
		strArrayListObj.add("Bbc");
		strArrayListObj.add("Abc");
		strArrayListObj.add("Rbc");
		
		
		Collections.sort(strArrayListObj);
		
		Collections.synchronizedList(strArrayListObj);
		
		for(String strobj: strArrayListObj){
			System.out.println("SortArrayListExample.main() " + strobj);
		}
		

	}

}
