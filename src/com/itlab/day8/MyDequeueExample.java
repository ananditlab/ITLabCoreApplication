package com.itlab.day8;

import java.util.ArrayDeque;
import java.util.Deque;

public class MyDequeueExample {
	public static void main(String args[]) {
		Deque<String> deQ = new ArrayDeque<String>();
		deQ.add("hello");
		deQ.add("world");
		deQ.add("java");
		deQ.add("is");
		deQ.add("interesting");
		System.out.println("DeQueue is" + deQ);
		System.out.println("Size before all the operations is:" + deQ.size());
		System.out.println("Remove element world:" + deQ.remove("world"));
		System.out.println("Remove first element:" + deQ.removeFirst());
		System.out.println("Remove last element:" + deQ.removeLast());
		System.out.println("Peek first element:" + deQ.peekFirst());
		System.out.println("Peek element:" + deQ.peek());
		System.out.println("Peek last element:" + deQ.peekLast());
		// System.out.println("Element added at first place:");
		deQ.addFirst("HIII!!");
		// System.out.println("Element added at last place:");
		deQ.addLast("BYE!!");
		System.out.println("Get first element:" + deQ.getFirst());
		System.out.println("Get last element:" + deQ.getLast());
		System.out.println("Size after all the operations is:" + deQ.size());
		System.out.println("DeQueue after all the operations is:" + deQ);
	}
}
