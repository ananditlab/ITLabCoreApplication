package com.itlab.day8;

import java.util.LinkedHashMap;
import java.util.Map;

public class MyLinkedHashMapExample {

	public static void main(String[] args) {
		Map<Number, String> myLinkedHashMap = new LinkedHashMap<Number, String>();
		myLinkedHashMap.put(1, "Hello");
		myLinkedHashMap.put(2, "friend");
		myLinkedHashMap.put(3, "hello");
		myLinkedHashMap.put(4, "world!");
		for(Object o:myLinkedHashMap.keySet()){
			System.out.println(o+"_"+myLinkedHashMap);
		}

	}

}
