package com.itlab.day8;

import java.util.HashMap;
import java.util.Map;

public class MyHashMapExample {
	public static void main(String args[]) {
		Map<Number, String> myMap = new HashMap<Number, String>();
		myMap.put(1, "hello");
		myMap.put(2, "world");
		myMap.put(3, "java");
		myMap.put(4, "is");
		myMap.put(5, "fun");
		for (Object o : myMap.keySet()) {
			System.out.println(o + "__" + myMap.get(o));
		}
	}

}
