package com.itlab.day8;

import java.util.HashSet;

public class MyHashSetExample {
	public static void main(String args[]) {
		HashSet<String> myHashSet = new HashSet<String>();
		myHashSet.add("heyy");
		myHashSet.add("hello");
		myHashSet.add("world");
		System.out.println("" + myHashSet);
	}
}
