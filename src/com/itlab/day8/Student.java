package com.itlab.day8;

public class Student {

	public String fname;
	public String lname;
	public int age;
	public String city;
	
	
	public Student(String fname, String lname, int age, String city) {
		super();
		this.fname = fname;
		this.lname = lname;
		this.age = age;
		this.city = city;
	}


	@Override
	public String toString() {
		return "Student [fname=" + fname + ", lname=" + lname + ", age=" + age + ", city=" + city + "]";
	}
	
	
	
	
}
