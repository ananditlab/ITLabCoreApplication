package com.itlab.day8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class CompartorImpForSortArray {

	public static void main(String[] args) {

		Student stuObj1 = new Student("Aanand", "Joshi", 35, "Pune");
		Student stuObj2 = new Student("Amit", "sds", 60, "mumbai");
		Student stuObj3 = new Student("AABBCC", "adsfd", 55, "maja");
		Student stuObj4 = new Student("Deepak", "dsfsdweww", 25, "solapur");
		Student stuObj5 = new Student("zinit", "wew", 77, "sangali");
		
		
		ArrayList<Student> arrayListObj = new ArrayList<Student>(Arrays.asList(stuObj1,stuObj2, stuObj3, stuObj4, stuObj5));
	
		Collections.sort(arrayListObj, new StudentSortByName());
		
		for (Student obj : arrayListObj){
			System.out.println("CompartorImpForSortArray.main() By Name" + obj.toString());
		}
		
		
		Collections.sort(arrayListObj, new StudentSortByAge());
		
		for (Student obj : arrayListObj){
			System.out.println("CompartorImpForSortArray.main() By Age" + obj.toString());
		}
		
	}

}
