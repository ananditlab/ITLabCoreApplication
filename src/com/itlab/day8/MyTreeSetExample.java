package com.itlab.day8;

import java.util.TreeSet;

public class MyTreeSetExample {
	public static void main(String args[]) {
		TreeSet<String> myTreeSet = new TreeSet<String>();
		myTreeSet.add("this is TreeSet");
		myTreeSet.add("Java is fun...");
		myTreeSet.add("hello");
		myTreeSet.add("world");
		System.out.println("Size of TreeSet is:" + myTreeSet.size());
		myTreeSet.size();
		System.out.println("last element is:" + myTreeSet.last());
		myTreeSet.last();
		System.out.println("first element is:" + myTreeSet.first());
		myTreeSet.first();
		System.out.println(""+myTreeSet);
	}
}
